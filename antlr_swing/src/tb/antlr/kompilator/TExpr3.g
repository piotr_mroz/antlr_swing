tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer number = 0;
  Integer counter = 0;
}
prog      : (e+=code | e+=expr | e+=comp | d+=decl)* -> program(name={$e},declarations={$d})
;

code : ^(BEGIN {enterScope();} (e+=code | e+=expr | e+=comp | d+=decl)* {leaveScope();}) -> block(statement={$e},declaration={$d})
;

decl      : ^(VAR i1=ID) {locals.newSymbol($ID.text);} -> declarate(p1={$ID.text})
;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr      : ^(PLUS  e1=expr e2=expr) -> plus(p1={$e1.st},p2={$e2.st})
	        | ^(MINUS e1=expr e2=expr) -> minus(p1={$e1.st},p2={$e2.st})
	        | ^(MUL   e1=expr e2=expr) -> mul(p1={$e1.st},p2={$e2.st})
	        | ^(DIV   e1=expr e2=expr) -> div(p1={$e1.st},p2={$e2.st})
	        | ^(ASSIGN i1=ID   e2=expr) {locals.hasSymbol($i1.text);} -> assign(p1={$ID},p2={$e2.st})
	        | INT                      -> int(i={$INT.text})
	        | ID                       {locals.checkSymbol($ID.text);} -> id(i={$ID.text})
	        | ^(WHILE while_processed=comp loop_block=code) {counter++;} -> while(while_processed={$while_processed.st},loop_block={$loop_block.st},nr={counter.toString()})
;        

comp      :  ^(EQ    e1=expr e2=expr) -> equal(p1={$e1.st},p2={$e2.st})
	        | ^(NEQ    e1=expr e2=expr) -> not_equal(p1={$e1.st},p2={$e2.st},nr={counter.toString()})
;
    
    