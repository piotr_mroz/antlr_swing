grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog 
    : (stat | block)+ EOF!
    ;

block 
    : NL! BEGIN^ NL! (stat | block)* END!
    ;

stat
    : expr NL -> expr
    | VAR ID (ASSIGN expr)? NL -> ^(VAR ID) ^(ASSIGN ID expr)?
    | ID ASSIGN expr NL -> ^(ASSIGN ID expr)
    | while_stat NL -> while_stat
    | NL ->
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;
    
while_expr
    : expr
      ( EQ^ expr
      | NEQ^ expr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

while_stat
	  : WHILE^ while_expr NL? block
	  ;
	  
  
VAR 
    : 'var'
    ;

WHILE
	  : 'while'
	  ;
  
BEGIN 
	  : '{'
	  ; 
  
END
	  : '}'
	  ;
  
EQ
	  : '=='
	  ;
  
NEQ
	  : '!='
	  ;
  
ID 
    : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
    ;

INT 
    : '0'..'9'+
    ;

NL 
    : '\r'? '\n' 
    ;

WS 
    : (' ' | '\t')+ {$channel = HIDDEN;} 
    ;

LP
    : '('
    ;

RP
    : ')'
    ;

ASSIGN
    : '='
    ;

PLUS
    : '+'
    ;

MINUS
    : '-'
    ;

MUL
    : '*'
    ;

DIV
    : '/'
    ;